import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Que implements Runnable {

    private int ID;
    private ArrayBlockingQueue<Client> client;
    private AtomicInteger simualtionTime;

    public Que(int ID,int client, AtomicInteger simualtionTime){
        this.ID=ID;
        this.client=new ArrayBlockingQueue<Client>(10);
        this.simualtionTime=new AtomicInteger(0);
    }

    public ArrayBlockingQueue<Client> getClient() { return client; }
    public AtomicInteger getSimualtionTime() { return simualtionTime; }
    public synchronized void addClient(Client client) {
     this.client.add(client);
    }

    public void run() {
        int verif=0;
        int endC;
        while(true){
            Client client=this.client.poll();
            if(client!=null) {verif = 1; }
            endC=0;
            while (endC==0 &&verif==1){

                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                client.dec();
                this.simualtionTime.getAndDecrement();

                if(client.getService().intValue()==0){
                    this.client.peek();verif=0;
                    }
                }
            }
        }
}

import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;


public class Scheduler {
    private int maxQueues;
    private int client;
    private AtomicInteger simulationTime;
    private ArrayList<Coada> queues;

    public Scheduler(int maxQueues,int client, AtomicInteger simulationTime) {
        this.maxQueues = maxQueues;
        this.client=client;
        this.simulationTime=simulationTime;
        this.queues=new ArrayList<Coada>(client);
        for(int k=1;k<=maxQueues;k++){
            Coada coada=new Coada(k, client , simulationTime );
            queues.add(coada);
        }
    }

    public ArrayList<Coada> getQueues() { return queues; }


    public void generateQueue(Client client){
        int q=0;
        queues.get(q).addClient(client);

    }
 }


import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

    public class FileParser {
        private ArrayList<Client> client;
        private int nrClients;
        private int nrQueues;
        private int timeMinProcessing;
        private int timeMaxProcessing;
        private int timeMinArrival;
        private int timeMaxArrival;
        private int timeLimit;


        public FileParser(ArrayList<Client> client,int nrQueues, int nrClients,int timeMinProcessing,int timeMaxProcessing ,int timeMinArrival, int timeMaxArrival,int timeLimit){
            super();
            this.client=client;
            this.nrQueues=nrQueues;
            this.nrClients=nrClients;
            this.timeMinProcessing=timeMinProcessing;
            this.timeMaxProcessing=timeMaxProcessing;
            this.timeMinArrival=timeMinArrival;
            this.timeMaxArrival=timeMaxArrival;
            this.timeLimit=timeLimit;
        }

        public FileParser() {

        }

        public ArrayList randomGenerate(ArrayList<Client> client){
            for(int i=1;i<=nrClients;i++){
                Random random=new Random();
                AtomicInteger arrivalTime=new AtomicInteger(random.nextInt(timeMaxArrival-timeMaxArrival+1)+timeMaxArrival);
                AtomicInteger serviceTime=new AtomicInteger(random.nextInt(timeMaxProcessing-timeMinProcessing+1)+timeMinProcessing);
                Client c=new Client(i,arrivalTime,serviceTime);
                client.add(c);
            }

            return client;
        }
        public void readFile(String name) {
            try {
                BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\Rebeca\\Tema2\\In-Test.txt"));
                nrClients = Integer.parseInt(in.readLine());
                System.out.println(nrClients);
                nrQueues = Integer.parseInt(in.readLine());
                System.out.println(nrQueues);
                timeLimit = Integer.parseInt(in.readLine());
                System.out.println(timeLimit);
                String[] arr = in.readLine().split(",");
                timeMinArrival = Integer.parseInt(arr[0]);
                System.out.println(timeMinArrival);
                timeMaxArrival = Integer.parseInt(arr[1]);
                System.out.println(timeMaxArrival);
                String[] proc = in.readLine().split(",");
                timeMinProcessing = Integer.parseInt(proc[0]);
                System.out.println(timeMinProcessing);
                timeMaxProcessing = Integer.parseInt(proc[1]);
                System.out.println(timeMaxProcessing);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
        public static void printFile(){
            try{
                FileWriter write=new FileWriter("Out-Test.txt");
                PrintWriter writer=new PrintWriter(write);
                writer.close();
                write.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }



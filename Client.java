import java.util.concurrent.atomic.AtomicInteger;

public class Client {
    private int ID;
    private  AtomicInteger arrival_time;
    private AtomicInteger service_time;


    public Client(int ID, AtomicInteger arrival_time, AtomicInteger service_time){
        this.ID=ID;
        this.arrival_time=arrival_time;
        this.service_time=service_time;
    }


    public AtomicInteger getArrival() { return arrival_time; }
    public AtomicInteger getService() { return service_time; }
    public void dec() {
        this.service_time.getAndDecrement();
    }
    public void inc() {
        this.service_time.getAndIncrement();
    }

    public String toString() {
        return "(" + ID +","+ arrival_time+ "," +  service_time+")";
    }
}

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Task implements Runnable{

    private Scheduler s;
    private ArrayList<Client> client;
    private AtomicInteger timeSimulation;
    private int nrQueues;
    private int nrClients;
    private int arrivalTime;
    private int processingTime;
    private int finishTime;
    private int waitingTime;

    public Task(AtomicInteger timeSimulation, int nrQueues, int nrClients ){
        this.timeSimulation=timeSimulation;
        this.nrQueues=nrQueues;
        this.nrClients=nrClients;
        this.s=new Scheduler(nrQueues,nrClients, timeSimulation);
        this.client=client;

    }
    public Task(){

    }

    public int getProcessingTime() {
        return processingTime;
    }



    private void afisare(int timp){
        String str="";
        str+="Time "+timp;
        str+="\n";
        str+="Waiting clients: ";
        if(client.size()==0){
            str+="closed!";
        }
        else{
            for(Client client:client){
                str+=client.toString();
            }
        }
        str+="\n";
        int i;
        for(i=0;i<nrQueues;i++){
            str+="Queue "+i+" ";
            if(s.getQueues().get(i).getClient().size()==0){
                str+="is closed!";
            }else{
                for(Client client: s.getQueues().get(i).getClient()){
                    str+=client.toString();
                }
            }
        }

    }

    public float averageTime() {
        int suma=0;
        for( Client client :client ) {
            suma=suma+getProcessingTime();
        }
        float average=suma/nrClients;
        return average;
    }

    private boolean verif(){
        boolean ClientVerif=false;
        boolean QueueVerif=false;

        if (client.size()!=0){  ClientVerif=true; }

        for(Coada q: s.getQueues()){
            if(q.getClient().size()!=0){ QueueVerif=true; }
        }
        if(ClientVerif==true || QueueVerif ==true){
            return true;
        }
        return true;
    }

    public void run(){
        int current=0;

        while(verif()==true ) {
            while ( client.size() > 0 && client.get(0).getArrival().intValue() == current ) {

                int q = 0;
                if (s.getQueues().get(q).getSimualtionTime().intValue() == 0) {
                    Thread thread = new Thread(s.getQueues().get(q));
                    s.generateQueue(client.get(0));
                    thread.start();
                } else if (s.getQueues().get(q).getSimualtionTime().intValue() > 0) {
                    client.get(0).inc();
                    s.generateQueue(client.get(0));
                }
                client.remove(client.get(0));
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            afisare(current);
            current++;
        }
        FileParser.printFile();
    }






}
